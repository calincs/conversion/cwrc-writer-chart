# CWRC Writer Deployment

A K8s deployment project for the stand-alone version of the CWRC GitWriter.

Based on the Docker Compose version here: [https://github.com/cwrc/CWRC-GitWriter-Docker](https://github.com/cwrc/CWRC-GitWriter-Docker)

The deployment contains gitwriter and gitserver images for CWRC.

It points to NERVE here: [https://nerve.lincsproject.ca/](https://nerve.lincsproject.ca/) and the XML validator here: [https://cwrc-writer.cwrc.ca/validator/validate.html](https://cwrc-writer.cwrc.ca/validator/validate.html)
